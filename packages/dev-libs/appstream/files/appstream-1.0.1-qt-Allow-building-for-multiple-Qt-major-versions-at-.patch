Upstream: yes

From 7af34d609d81c1f9203d28efe1bc58e4f91448af Mon Sep 17 00:00:00 2001
From: Matthias Klumpp <matthias@tenstral.net>
Date: Mon, 18 Dec 2023 21:33:12 +0100
Subject: [PATCH] qt: Allow building for multiple Qt major versions at once

---
 meson.build          |  2 +-
 meson_options.txt    | 11 ++++++-----
 qt/meson.build       | 21 ++++++++++++++-------
 qt/tests/meson.build | 15 +++++++++++++--
 4 files changed, 34 insertions(+), 15 deletions(-)

diff --git a/meson.build b/meson.build
index 487f9031..fa171d53 100644
--- a/meson.build
+++ b/meson.build
@@ -219,6 +219,6 @@ subdir('data/')
 subdir('contrib/')
 subdir('docs/')
 subdir('tests/')
-if get_option('qt') or get_option('qt5')
+if get_option('qt')
     subdir('qt/')
 endif
diff --git a/meson_options.txt b/meson_options.txt
index b650fa2b..6964e47a 100644
--- a/meson_options.txt
+++ b/meson_options.txt
@@ -16,12 +16,13 @@ option('vapi',
 option('qt',
        type : 'boolean',
        value : false,
-       description : 'Build libappstream-qt for Qt6'
+       description : 'Build libAppStreamQt'
 )
-option('qt5',
-       type : 'boolean',
-       value : false,
-       description: 'Build libappstream-qt5 for Qt5'
+option('qt-versions',
+       type: 'array',
+       choices: ['5', '6'],
+       value : ['6'],
+       description: 'Major versions of Qt to build for.'
 )
 option('compose',
        type : 'boolean',
diff --git a/qt/meson.build b/qt/meson.build
index 4efcfa18..1526c360 100644
--- a/qt/meson.build
+++ b/qt/meson.build
@@ -1,12 +1,10 @@
 # Meson definition for AppStream Qt
 
-if get_option('qt') and get_option('qt5')
-    error('Both the "qt" and "qt5" option enabled. Please select only one Qt major version to build!')
-endif
-
 asqt_api_level = '3'
 
-if get_option('qt5')
+foreach qt_major : get_option('qt-versions')
+
+if qt_major == '5'
     qt_major_version = '5'
     qt_min_version = '5.15'
     asqt_library_name = 'AppStreamQt5'
@@ -108,6 +106,12 @@ appstreamqt_lib = library(f'@asqt_library_name@',
     install: true
 )
 
+if qt_major == '5'
+    appstreamqt5_lib = appstreamqt_lib
+else
+    appstreamqt6_lib = appstreamqt_lib
+endif
+
 configure_file(
     input: 'version.h.in',
     output: 'version.h',
@@ -117,8 +121,6 @@ configure_file(
 
 install_headers(asqt_pub_headers, subdir: f'@asqt_library_name@')
 
-subdir('tests/')
-
 #
 # CMake support
 #
@@ -142,3 +144,8 @@ install_data (
     join_paths(meson.current_build_dir(), f'@asqt_library_name@ConfigVersion.cmake'),
     install_dir: join_paths(get_option('libdir'), 'cmake', asqt_library_name)
 )
+
+# end of Qt version loop
+endforeach
+
+subdir('tests/')
diff --git a/qt/tests/meson.build b/qt/tests/meson.build
index 0ff708b6..7cd12a23 100644
--- a/qt/tests/meson.build
+++ b/qt/tests/meson.build
@@ -1,5 +1,8 @@
 # Meson definition for AppStream Qt Tests
 
+foreach qt_major_version : get_option('qt-versions')
+
+qt = import(f'qt@qt_major_version@')
 qt_test_dep = dependency(
     f'qt@qt_major_version@',
     modules: ['Core', 'Test'],
@@ -16,7 +19,13 @@ asqt_test_src = [
 
 asqt_test_moc = qt.preprocess (moc_sources: asqt_test_src)
 
-as_test_qt_exe = executable ('as-test_qt',
+if qt_major_version == '5'
+    appstreamqt_lib = appstreamqt5_lib
+else
+    appstreamqt_lib = appstreamqt6_lib
+endif
+
+as_test_qt_exe = executable(f'as-test_qt@qt_major_version@',
     [asqt_test_src,
      asqt_test_moc],
     dependencies: [qt_test_dep],
@@ -24,7 +33,9 @@ as_test_qt_exe = executable ('as-test_qt',
     link_with: [appstreamqt_lib],
     cpp_args: asqt_cpp_args,
 )
-test ('as-test_qt',
+test(f'as-test_qt@qt_major_version@',
     as_test_qt_exe,
     env: as_test_env
 )
+
+endforeach
-- 
2.43.0

