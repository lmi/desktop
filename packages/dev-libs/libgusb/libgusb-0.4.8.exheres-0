# Copyright 2012 Benedikt Morbach <benedikt.morbach@googlemail.com>
# Distributed under the terms of the GNU General Public License v2

require github [ user=hughsie release=${PV} suffix=tar.xz ] \
    meson \
    python [ blacklist=2 multibuild=false with_opt=true option_name=gobject-introspection ] \
    vala [ with_opt=true option_name=vapi vala_dep=true ]

SUMMARY="GObject wrapper for libusb1"

LICENCES="LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64 ~armv8"
MYOPTIONS="
    gobject-introspection
    gtk-doc
    vapi [[ requires = [ gobject-introspection ] ]]
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
"

# tests try to access usb devices
RESTRICT="test"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.9.0]
        gobject-introspection? (
            dev-python/setuptools:*[python_abis:*(-)?] [[ note = [ for pkg_resources ] ]]
            gnome-desktop/gobject-introspection:1[>=1.29][python_abis:*(-)?]
        )
        gtk-doc? ( dev-doc/gi-docgen[>=2021.1] )
    build+run:
        core/json-glib[>=1.1.1][gobject-introspection?]
        dev-libs/glib:2[>=2.44.0]
        dev-libs/libusb:1[>=1.0.22]
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd )
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dumockdev=disabled
    -Dusb_ids=/usr/share/misc/usb.ids
)
MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'gobject-introspection introspection'
    'gtk-doc docs'
    vapi
)
MESON_SRC_CONFIGURE_TESTS=(
    '-Dtests=true -Dtests=false'
)

src_prepare() {
    meson_src_prepare

    # Respect selected python abi
    if option gobject-introspection ; then
        edo sed \
            -e "s:pymod.find_installation():pymod.find_installation('python$(python_get_abi)'):g" \
            -i gusb/meson.build
    fi
}

