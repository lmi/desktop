# Copyright 2009 Saleem Abdulrasool <compnerd@compnerd.org>
# Copyright 2013 Johannes Nixdorf <mixi@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github meson test-dbus-daemon
require vala [ with_opt=true option_name=vapi vala_dep=true ]

SUMMARY="A library that provides automatic proxy configuration management"

LICENCES="LGPL-2.1"
SLOT="1"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    duktape [[ description = [ Use the Duktape javascript engine for PAC ] ]]
    gnome [[ description = [ Add support for using GNOME3 settings to determine the correct proxy ] ]]
    gobject-introspection
    gtk-doc [[ requires = [ gobject-introspection ] ]]
    kde [[ description = [ Add support for using KDE settings to determine the correct proxy ] ]]
    vapi [[ requires = [ gobject-introspection ] ]]
"

# environment test wants to connect to dbus system_bus_socket
RESTRICT="test"

DEPENDENCIES="
    build:
        virtual/pkg-config
        gtk-doc? ( dev-doc/gi-docgen )
    build+run:
        dev-libs/glib:2[>=2.71.3]
        duktape? ( dev-libs/duktape )
        gnome? ( gnome-desktop/gsettings-desktop-schemas )
        gobject-introspection? ( gnome-desktop/gobject-introspection:1 )
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dconfig-env=true
    -Dconfig-osx=false
    -Dconfig-sysconfig=true
    -Dconfig-windows=false
    -Dcurl=false
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'duktape pacrunner-duktape'
    'gnome config-gnome'
    'gobject-introspection introspection'
    'gtk-doc docs'
    'kde config-kde'
    vapi
)

MESON_SRC_CONFIGURE_TESTS=(
    '-Dtests=true -Dtests=false'
)

src_test() {
    unset DISPLAY
    unset DBUS_SESSION_BUS_ADDRESS
    unset DBUS_SESSION_BUS_PID

    test-dbus-daemon_run-tests meson_src_test
}

